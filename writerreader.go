package socket

import (
	"net"
)

//WriterReader interface for tcp and web sockets
type WriterReader interface {
	Write(c net.Conn, b []byte) (int, error)
	Read(c net.Conn) ([]byte, error)
}
