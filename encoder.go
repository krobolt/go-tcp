package socket

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	mrand "math/rand"
	"strings"
	"time"
)

type encoder struct {
	key string
}

func NewTransport() *encoder {
	Seed()
	return &encoder{
		key: GenKey(64),
	}
}

func (t *encoder) Pack(s string) (string, error) {
	msg, err := EncMsg(s, t.key)
	return msg, err
}

func (t *encoder) Unpack(s string) (string, error) {
	msg, err := DecMsg(s, t.key)
	return msg, err
}

func Seed() {
	mrand.Seed(time.Now().UnixNano())
}
func GenKey(n int) string {
	// 6368616e676520746869732070617373
	r := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "e", "f"}
	out := make([]string, 0)
	for i := 0; i < n; i++ {
		out = append(out, r[mrand.Intn(15)])
	}
	return strings.Join(out, "")
}

func NewKey(secret string) []byte {
	key, _ := hex.DecodeString(secret)
	return key
}

func EncMsg(message string, secret string) (string, error) {

	key, _ := hex.DecodeString(secret)
	plaintext := []byte(message)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// It's important to remember that ciphertexts must be authenticated
	// (i.e. by using crypto/hmac) as well as being encrypted in order to
	// be secure.
	return fmt.Sprintf("%x", ciphertext), nil

}

func DecMsg(message string, secret string) (string, error) {

	key, _ := hex.DecodeString(secret)
	ciphertext, _ := hex.DecodeString(message)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)
	return string(ciphertext), nil
}

func transexample() {

	transport := NewTransport()
	s, _ := transport.Pack(`	
		
		11Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elit mi, scelerisque a sodales ac, vestibulum sed sem. 
		Integer eget hendrerit tortor, quis faucibus dui. Nam mi augue, gravida quis sapien a, tempus ullamcorper tortor. 
		Cras a pellentesque urna, ut mollis ex. Morbi posuere tortor et mi molestie, vitae molestie lorem blandit. Fusce ultricies bibendum arcu,
		a sollicitudin erat ornare a. Fusce mauris magna, consequat id sollicitudin sit amet, pharetra nec lorem. Vestibulum ac lectus nec
		felis maximus gravida quis sed neque. Mauris nibh diam, condimentum et iaculis vel, tincidunt sed ligula. Sed vitae tempor ex. 
		Pellentesque odio magna, commodo et diam in, fermentum semper magna.

		Etiam pretium est pharetra felis finibus, in posuere leo consectetur. Aliquam lorem ex, congue a arcu et, commodo auctor ex. 
		Suspendisse at nulla eu ante consequat fringilla nec a quam. Fusce placerat ligula justo, sit amet tempor nisl scelerisque in. Duis mauris lectus,
		molestie non turpis non, egestas placerat velit. Maecenas a laoreet nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus.
		In sagittis semper velit quis pharetra. Fusce massa ligula, pulvinar id semper sit amet, viverra a magna. Sed euismod ex condimentum, feugiat felis vel, mattis dui. Donec bibendum nunc sit amet lectus laoreet feugiat. Nunc blandit ante vel placerat tincidunt. Maecenas pellentesque eu massa vel sollicitudin. Donec lobortis tortor non urna pretium, eu vulputate velit facilisis. Sed orci magna, suscipit eu mauris sit amet, faucibus dapibus ligula.

		Nunc accumsan tellus nisi, ac egestas velit sagittis sit amet. Proin id eleifend velit. Aenean purus nibh, venenatis ut elit at, interdum mollis arcu. 
		Proin sed lacus eu arcu commodo sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam a lectus quis turpis posuere maximus
		in id leo. Nullam elementum velit lorem, et posuere orci euismod in. Praesent felis risus, tincidunt quis molestie ac, suscipit sit amet sapien. 
		Curabitur dignissim orci nisi, ac facilisis metus auctor quis. Fusce ut nunc vel sem rhoncus laoreet sit amet sit amet eros. Ut volutpat lorem non
		lacus euismod condimentum.

		Nam gravida dictum suscipit. Integer nec tempor magna. Ut suscipit, leo eu feugiat eleifend, mauris nulla rhoncus nibh, vitae efficitur augue elit non nunc.
		Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed mollis lorem in elit pulvinar, vel aliquet enim sodales. 
		Nam dictum lacus dolor, at lacinia nulla lacinia non. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed tempus vitae sapien nec mattis.
		Vestibulum sit amet pulvinar dui, in sodales dolor. Cras venenatis tempus ligula, ut gravida ipsum aliquam ac.

		Aliquam in diam a enim mattis lacinia id eu augue. Nam vitae turpis augue. Nam vel risus id orci imperdiet finibus sit amet sed ex. Sed nec dui imperdiet,
		volutpat quam sed, maximus nisi. Vivamus bibendum elit in urna elementum, vitae tincidunt urna lacinia. Praesent ultrices velit non neque rutrum finibus.
		In hac habitasse platea dictumst. Etiam lectus nulla, lobortis eleifend pellentesque sit amet, lobortis eu nisi. Duis urna dolor, tincidunt sit amet enim nec,
		elementum finibus eros. Aliquam eget hendrerit nunc, eu vulputate massa. Proin quis libero tincidunt felis auctor feugiat. 
		Fusce dui orci, suscipit eu nunc vitae, ornare faucibus arcu. Vestibulum nec consequat urna, tempus ullamcorper erat. Nam vitae neque ut sapien
		iaculis sollicitudin sit amet a lacus. Integer suscipit, purus in pharetra consequat, nisi sem malesuada leo, tempor facilisis turpis lacus sit amet mauris.
		Fusce fermentum finibus sollicitudin.22

	`)
	r, _ := transport.Unpack(s)
	fmt.Println(s, r)

}
