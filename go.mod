module gitlab.com/krobolt/go-tcp

go 1.13

require (
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4
	gitlab.com/krobolt/go-skeleton v0.0.0-20210218230944-5be12d792c0a
	golang.org/x/crypto v0.0.0-20210218145215-b8e89b74b9df
)
