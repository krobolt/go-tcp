package socket

import (
	"errors"
	"log"
	"net"
)

//Room struct
type Room struct {
	Name        string
	Connections map[net.Conn]*Connection
	Password    string
	*Coms
}

//NewRoom creates a new room
func NewRoom(name string, password string) *Room {
	return &Room{
		name,
		make(map[net.Conn]*Connection),
		password,
		NewComs(),
	}
}

//Auth authenticate request
func (r *Room) Auth(password string) bool {
	if password == r.Password {
		return true
	}
	return false
}

//Find username in room and return Connection if found
func (r *Room) Find(username string) (*Connection, error) {
	for _, c := range r.Connections {
		if c.Username == username {
			return c, nil
		}
	}
	return nil, errors.New("unable to find connection")
}

//Relay recieves/proceses signals from Server chans
func (r *Room) Relay(s *Server) {
	defer func() {
		log.Println("closing room relay: ", r.Name)
		close(r.Close)
		close(r.Disconnect)
		close(r.Connect)
		close(r.Broadcast)
		r.Connections = nil
	}()

	for {
		select {
		case conn := <-r.Connect:
			//add connection to room
			//add room to connection
			r.Connections[conn.Conn] = conn
			conn.Rooms[r.Name] = true
			s.Log.Debugf("added new connection to room, total connections: %v", len(r.Connections))
			//send message to users in room
			go func() {
				//send to everybody even the sender
				r.Announce <- Message{
					Code:     ConnectStatusCode,
					Contents: []string{conn.Username + " has joined the room"},
				}
			}()
			break
		case conn := <-r.Disconnect:
			delete(r.Connections, conn.Conn)
			delete(conn.Rooms, r.Name)
			//clean up and close the room if there are no connections present
			s.Log.Debugf("removing connection, total connections: %v", len(r.Connections))

			switch len(r.Connections) {
			case 0:
				s.Log.Debugf("request room shutdown: %s", r.Name)

				go func() {
					//request shutdown
					s.Close <- r.Name
				}()
				break
			default:
				//connections present so let other connections know that this connection has disconnected
				go func() {

					r.Announce <- Message{
						Code:     DisconnectCode,
						Contents: []string{conn.Username + " has left the room"},
					}

				}()
			}
			break
		case announce := <-r.Announce:
			res, err := WriteMessage(Response{
				Code:     announce.Code,
				Room:     r.Name,
				From:     r.Name,
				Contents: announce.Contents,
			})
			if err != nil {
				break
			}
			for _, c := range r.Connections {
				s.Log.Debugf("sending message to client: %s", c.Conn.LocalAddr())
				s.Log.Debugf("message: %s", res)
				s.Log.Debugf("len: %v", len(res))
				s.Socket.Write(c.Conn, res)
			}
			break
		case ping := <-r.Ping:
			res, err := WriteMessage(Response{
				Code:     ping.Code,
				Room:     r.Name,
				From:     ping.From.Username,
				Contents: ping.Contents,
			})
			if err != nil {
				break
			}
			s.Socket.Write(ping.To.Conn, res)
			break
		case broadcast := <-r.Broadcast:
			res, err := WriteMessage(Response{
				Code:     broadcast.Code,
				Room:     r.Name,
				From:     broadcast.From.Username,
				Contents: broadcast.Contents,
			})
			if err != nil {
				break
			}
			s.Log.Debugf("broadcast message room: %s", r.Name)
			for _, c := range r.Connections {
				if c.Conn != broadcast.From.Conn {
					s.Socket.Write(c.Conn, res)
				}
			}
			break
		case room := <-r.Close:
			if len(r.Connections) == 0 {
				s.Log.Warnf("room: %s. closed", r.Name)
				go func() {
					//send close back to server
					// to clean up available rooms
					s.Close <- room
				}()
				//close
				return
			}
		}
	}
}
