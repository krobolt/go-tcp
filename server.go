package socket

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

//Server handles tcp connection and global messages
//All other messages are handled by room.go
type Server struct {
	ID          string
	Servers     map[string]Server
	Config      *Config
	Connections map[*Connection]*Connection
	Rooms       map[string]*Room
	CloseChan   chan *Connection
	Socket      WriterReader
	Procol      *Protocol
	Log         *Log
	*Coms
}

//NewServer returns instance of Server
func NewServer(config *Config, socket WriterReader, logfile *Log) *Server {
	return &Server{
		GenKey(32),
		make(map[string]Server),
		config,
		make(map[*Connection]*Connection),
		make(map[string]*Room),
		make(chan *Connection),
		socket,
		NewChatProtocol(),
		logfile,
		NewComs(),
	}
}

//NewTCPServer creates instance using default TCPSocket
func NewTCPServer(config *Config, logger *Log) {

	l, err := tls.Listen("tcp", config.Address, config.TLSConfig)
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()

	s := NewServer(
		config,
		&TCPSocket{
			WriteDeadline: config.WriteDeadline,
			ReadDeadline:  config.ReadDeadline,
		},
		logger,
	)

	go s.Listen(l)
	go s.Relay()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	_, cancel := context.WithTimeout(context.Background(), config.Wait)
	defer cancel()
	s.Log.Warnf("shutting down [%v]", time.Now().Unix())
	s.Shutdown()
	time.Sleep(config.Wait)
	os.Exit(0)
}

//Listen accepts and handles new net.Listener connections.
func (s *Server) Listen(l net.Listener) {
	for {
		for {
			conn, err := l.Accept()
			if err != nil {
				s.Log.Critf("unable to acception connection: %s", err.Error())
				break
			}
			s.Log.Debugln("new connection accepted, handling connection:")
			go s.Handle(conn)
		}
	}
}

//Handle new connection
func (s *Server) Handle(conn net.Conn) {
	c := NewConnection(conn.LocalAddr().String(), conn)
	s.Log.Debugf("new connection: [%s]", c.Conn.RemoteAddr().String())
	defer func() {
		s.Log.Debugf("connection [%s] handled", c.Conn.RemoteAddr().String())
		s.CloseConnection(c)
		conn.Close()
	}()

	mointorConnection(s, c)
}

//Relay recieves/proceses signals from Server chans
func (s *Server) Relay() {
	defer func() {
		s.Log.Critln("ServerHub relay terminated, connections no longer possible")
		s.Shutdown()
	}()
	s.Log.Debugln("Begin Server Relayer")
	for {
		select {
		case <-s.Connect:
			//TODO:(*Connection) connect new servers to pool

		case <-s.Disconnect:
			//TODO:(*Connection) disconnect server from pool

		case ping := <-s.Ping:
			switch ping.Data {
			//handle any internal messages ping.Data = protocol.ServerMessage
			case ServerMessage:

				b, err := json.Marshal(Response{
					Code:     ping.Code,
					Room:     "server",
					From:     "server",
					Contents: ping.Contents,
				})

				if err != nil {
					s.Log.Debugln(err.Error())
					continue
				}

				s.Socket.Write(ping.To.Conn, b)
			case ListCommand:

				ping.Contents = []string{}

				if !s.InRoom(ping.To, ping.Room) {
					continue
				}

				for _, conn := range s.Rooms[ping.Room].Connections {
					if conn.Username != ping.To.Username {
						ping.Contents = append(ping.Contents, conn.Username)
					}
				}
				data, err := WriteMessage(Response{
					Code:     ping.Code,
					Room:     ping.Room,
					From:     s.ID,
					Contents: ping.Contents,
				})

				if err != nil {
					s.Log.Debugln(err.Error())
					continue
				}

				s.Socket.Write(ping.To.Conn, data)
			default:
				//send to room for processing
				if !s.InRoom(ping.To, ping.Room) {
					continue
				}
				if room, ok := s.Rooms[ping.Room]; ok {
					s.Rooms[room.Name].Ping <- ping
				}
			}
		case broadcast := <-s.Broadcast:
			if !s.InRoom(broadcast.From, broadcast.Room) {
				continue
			}
			if _, ok := s.Rooms[broadcast.Room]; ok {
				s.Rooms[broadcast.Room].Broadcast <- broadcast
			}

		case room := <-s.Close:
			//close room
			//remove from room list
			if _, ok := s.Rooms[room]; ok {
				if len(s.Rooms[room].Connections) == 0 {
					delete(s.Rooms, room)
				}
			}
		case c := <-s.CloseChan:
			//TODO: bug close already closed room results in panic
			s.CloseConnection(c)
		}
	}
}

//CloseConnection broadcast disconnected messages and close conn
func (s *Server) CloseConnection(c *Connection) {
	defer func() {
		s.Log.Debugf("Server Close: Connection: %s Closed", c.Conn.RemoteAddr().String())
		//delete ref to connection
		//close the connection
		delete(s.Connections, c)
		c.Conn.Close()
	}()
	for name, ok := range c.Rooms {
		if ok {
			go func() {
				s.Rooms[name].Disconnect <- c
			}()
		}
	}
}

//CreateRoom Create room if not found and begin listening
func (s *Server) CreateRoom(room string, password string) {
	if _, ok := s.Rooms[room]; ok {
		return
	}
	s.Log.Debugf("New room created: %s", room)
	s.Rooms[room] = NewRoom(room, password)
	go s.Rooms[room].Relay(s)
}

//GetPacket extracts message information from bytes
func (s *Server) GetPacket(c *Connection, b []byte) (*Packet, error) {

	var allowed = false

	p, err := GeneratePacket(c, b)
	if err != nil {
		return nil, err
	}

	for _, command := range s.Procol.Cmd() {

		if p.Cmd == command.Name {
			allowed = true
			if command.Min > 0 {
				if len(p.Args) < command.Min {
					return nil, errors.New("reject command incorrect min arg len")
				}
			}

			if command.Max > 0 {
				if len(p.Args) > command.Max {
					return nil, errors.New("reject command incorrect max arg len")
				}
			}

			if command.Whitelisted == false {
				s.IllegalInstruction(c)
				return nil, fmt.Errorf("not authorized to send command: %s", p.Cmd)
			}
		}
	}

	if !allowed {
		s.IllegalInstruction(c)
		return nil, fmt.Errorf("reject command not found %s", p.Cmd)
	}

	return p, nil
}

//Process reads input and passes relavent to the server
func (s *Server) Process(c *Connection, msg []byte) {

	s.Log.Critf("recived message %s", msg)
	packet, err := s.GetPacket(c, msg)
	if err != nil {
		//malformed packet/unrec command. Reject
		s.Log.Critln(err.Error())
		return
	}

	switch packet.Cmd {
	case HandshakeCommand:
		s.Ping <- Message{
			To:       c,
			Data:     ServerMessage,
			Code:     HandshakeStatusCode,
			Contents: []string{"pong"},
		}
		break
	case LoginCommand:
		//[/login] [username] [password]
		if len(packet.Args) > 1 {
			username := packet.Args[0]
			password := packet.Args[1]
			s.Log.Debugln(username + ", " + password)
		}
		break
	case LogoutCommand:
		//[/logout]
		packet.From.Username = ""
		packet.From.Rooms = make(map[string]bool)
		s.CloseConnection(c)
		break
	case JoinCommand:
		//[/join] [room] ([password])

		go s.JoinRoom(packet)
		break
	case LeaveCommand:
		//[/leave] [room]
		go s.LeaveRoom(packet)
		break
	case SendCommand:
		// [/send] [room] [usernamme] [msg.....]
		if len(packet.Args) < 3 {
			//not a valid request, reject
			//must container room, user and message
			return
		}
		packet.Room = packet.Args[0]
		if !s.InRoom(c, packet.Room) {
			return
		}

		msg := []string{strings.Join(packet.Args[2:len(packet.Args)], " ")}[0:0]
		toConn, err := s.Rooms[packet.Room].Find(packet.Args[1])

		if err != nil {
			return
		}

		s.Rooms[packet.Room].Ping <- Message{
			From:     c,
			To:       toConn,
			Code:     SendStatusCode,
			Contents: msg,
		}
		break
	case BroadcastCommand:
		// [/broadcast] [room] [msg.....]
		packet.Room = packet.Args[0]
		if !s.InRoom(c, packet.Room) {
			return
		}
		msg := []string{strings.Join(packet.Args[1:len(packet.Args)], " ")}[0:0]

		s.Broadcast <- Message{
			From: c,
			//TO: not used broadcast to everybody but from
			Room:     packet.Room,
			Code:     BroadcastStatusCode,
			Contents: msg,
		}
		break
	case ListCommand:
		// [/list] [room]
		if !s.InRoom(c, packet.Room) {
			return
		}
		//list connections
		s.Rooms[packet.Room].Ping <- Message{
			Data: ListCommand,
			To:   packet.From,
			Room: packet.Room,
			Code: ListStatusCode,
		}
		break
	default:
		//should already be filtered before getting to this point.
		log.Printf("received unknown cmd:[%s] with args: %v from connection: %s \n", packet.Cmd, packet.Args, c.Conn.RemoteAddr().String())
	}
}

//InRoom check if connection is already in room
func (s *Server) InRoom(c *Connection, room string) bool {
	if _, ok := s.Rooms[room].Connections[c.Conn]; ok {
		return true
	}
	s.Log.Warnf("unable to broadcast to room %s connection %s not found", room, c.Conn.RemoteAddr().String())
	return false
}

//JoinRoom from packet information
func (s *Server) JoinRoom(p *Packet) {

	var (
		room = p.Args[0]
		pw   = ""
	)

	if _, ok := s.Rooms[room]; !ok {
		s.CreateRoom(room, "")
	}

	//public rooms has empty string as password
	//use pw if provided /join roomname password
	if len(p.Args) > 1 {
		pw = p.Args[1]
	}

	if !s.Rooms[room].Auth(pw) {
		s.Ping <- Message{
			To:       p.From,
			Data:     ServerMessage,
			Code:     500,
			Contents: []string{"unable to connect incorrect password for room"},
		}
		return
	}

	s.Rooms[room].Connect <- p.From
	return
}

//LeaveRoom based on packet data
func (s *Server) LeaveRoom(p *Packet) {
	if _, ok := s.Rooms[p.Args[0]]; ok {
		s.Rooms[p.Args[0]].Disconnect <- p.From
	}
}

//IllegalInstruction logs and increase connection Illegal opperation count
//Kick the connection if maxIllegalOpsPerConnection is reached
//TODO: remove global
func (s *Server) IllegalInstruction(c *Connection) {
	c.Health.IllegalOps++
	if c.Health.IllegalOps > s.Config.MaxIllegalOps {
		log.Printf("kicking bad connection: %s \n", c.Conn.RemoteAddr().String())
		s.CloseConnection(c)
	}
}

//Shutdown server
func (s *Server) Shutdown() {
	//close connections
	for _, c := range s.Connections {
		s.Log.Debugf("terminating connection [%s]", c.Conn.RemoteAddr().String())
		s.CloseConnection(c)
	}
	//close any open rooms
	for room := range s.Rooms {
		s.Close <- room
	}
}

//UpgradeRequest Websocket upgrade, returns net.Conn
func (s *Server) UpgradeRequest(w http.ResponseWriter, r *http.Request) (net.Conn, error) {
	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

//BridgeWebsocketToTCP pass connection to an exisiting server
func (s *Server) BridgeWebsocketToTCP(conn net.Conn, network, address string, config *tls.Config) {
	proxy, err := tls.Dial(network, address, config)
	if err != nil {
		s.Log.Critln(err.Error())
		return
	}
	defer func() {
		s.Log.Debugln("handled websocket proxied to tcp connection")
		conn.Close()
		proxy.Close()
	}()
	close := make(chan int)
	go s.proxyWebsocketToTCP(conn, proxy, close)
	go s.proxyTCP(proxy, conn, close)
	<-close
}

func (s *Server) proxyWebsocketToTCP(incomming net.Conn, outgoing net.Conn, close chan int) {

	tcp := &TCPSocket{
		WriteDeadline: s.Config.WriteDeadline,
		ReadDeadline:  s.Config.ReadDeadline,
	}

	for {
		msg, _, err := wsutil.ReadClientData(incomming)
		if err != nil {
			s.Log.Critln(err.Error())
			close <- 1
			return
		}
		tcp.Write(outgoing, msg)
	}
}

func (s *Server) proxyTCP(src net.Conn, dst net.Conn, close chan int) {

	tcp := &TCPSocket{
		WriteDeadline: s.Config.WriteDeadline,
		ReadDeadline:  s.Config.ReadDeadline,
	}

	//cbuf := bufio.NewReader(src)
	for {

		msg, err := tcp.Read(src)

		//TODO: use tcp/websocket read functions
		//bug raw string
		//get header/message size from uint16 LittleEndian bytes
		/*
			b1, e := cbuf.ReadByte()
			b2, e := cbuf.ReadByte()

			if e != nil {
				s.Log.Debugln(e.Error())
				return
			}

			datalen := binary.LittleEndian.Uint16([]byte{b1, b2})
			if datalen == 0 {
				s.Log.Debugln("connection not available")
				return
			}
			buf := make([]byte, int(datalen))

			s.Log.Critf("proxy read header: %v", datalen)

			//read full message
			_, e = cbuf.Read(buf)

			//buf, e := tcp.Read(src)
		*/

		if err != nil {
			s.Log.Debugln(err.Error())
			return
		}

		s.Log.Debugf("proxy foward request: %s", msg)
		s.Socket.Write(dst, msg)

	}
}

//BridgeTCPToTCP pass connection to an exisiting server
func (s *Server) BridgeTCPToTCP(conn net.Conn, network, address string, config *tls.Config) {
	proxy, err := tls.Dial(network, address, config)
	if err != nil {
		s.Log.Critln(err.Error())
		return
	}
	defer func() {
		s.Log.Debugln("handled tcp proxied to tcp connection")
		conn.Close()
		proxy.Close()
	}()
	close := make(chan int)
	go s.proxyTCP(conn, proxy, close)
	go s.proxyTCP(proxy, conn, close)
	<-close
}

//mointorConnection read socket data and process results
func mointorConnection(s *Server, c *Connection) {
	defer func() {
		s.Log.Debugf("closing connection relay: %s", c.Conn.RemoteAddr().String())
		s.CloseConnection(c)
	}()
	s.Log.Debugf("starting connection relay: %s", c.Conn.RemoteAddr().String())

	for {
		msg, e := s.Socket.Read(c.Conn)
		if e != nil {
			if e == io.EOF {
				s.Log.Critln("Connection Unavilable/Closed")
				s.Disconnect <- c
			}
			s.Log.Critln(e.Error())
			return
		}

		s.Log.Debugf("full msg %s", msg)
		s.Process(c, msg)
	}
}
