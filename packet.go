package socket

import (
	"encoding/base64"
	"strings"
)

type Packet struct {
	From *Connection //c
	Room string
	Cmd  string
	Args []string //import arguments
	Hash string
}

//NewPacket create default packet
func NewPacket(cmd string, args []string) *Packet {
	return &Packet{
		Cmd:  cmd,
		Args: args,
	}
}

//GeneratePacket from bytes
func GeneratePacket(c *Connection, b []byte) (*Packet, error) {
	packet := NewPacket("error", []string{})
	packet.From = c
	packet.Hash = stripmessage(b)
	//args, err := decodehash(packet.Hash)
	//if err != nil {
	//	return packet, err
	//}
	request := strings.Split(string(b), " ")
	packet.Cmd = strings.Replace(request[0], "/", "", 1)
	packet.Args = request[1:]
	return packet, nil
}

func decodehash(s string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(s)
}

func stripmessage(b []byte) string {
	return strings.Split(string(b[:len(b)]), ";")[0]
}
