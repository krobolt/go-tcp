package socket

import (
	"log"
)

const (
	//LogDebug error level
	LogDebug = 100
	//LogAlert error level
	LogAlert = 200
	//LogWarn error level
	LogWarn = 300
	//LogCritical error level
	LogCritical = 400
)

//Log struct with levels
type Log struct {
	Level  int
	prefix string
	suffix string
}

//NewLogger creates new log instance with debug level
func NewLogger(level int) *Log {
	return &Log{
		Level:  level,
		suffix: "\n",
	}
}

//Debugf message with vars
func (l *Log) Debugf(s string, v ...interface{}) {
	l.prefix = "DEBUG: "
	if l.Level >= LogDebug {
		log.Printf(l.prefix+s+l.suffix, v...)
	}
}

//Debugln message
func (l *Log) Debugln(s string) {
	l.prefix = "DEBUG: "
	if l.Level >= LogDebug {
		log.Println(l.prefix + s + l.suffix)
	}
}

//Warnf warning log message
func (l *Log) Warnf(s string, v ...interface{}) {
	l.prefix = "WARNING: "
	if l.Level > LogWarn {
		log.Printf(l.prefix+s+l.suffix, v...)
	}
}

//Warnln critical warning log message
func (l *Log) Warnln(s string) {
	l.prefix = "WARNING: "
	if l.Level > LogWarn {
		log.Printf(l.prefix + s + l.suffix)
	}
}

//Critln critical warning log message
func (l *Log) Critln(s string) {
	l.prefix = "CRITICAL: "
	log.Printf(l.prefix + s + l.suffix)
}

//Critf critical warning log message
func (l *Log) Critf(s string, v ...interface{}) {
	l.prefix = "CRITICAL: "
	log.Printf(l.prefix+s+l.suffix, v...)
}
