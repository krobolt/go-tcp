package socket

const (
	HandshakeCommand    = "handshake"
	HandshakeStatusCode = 0
	LoginCommand        = "login"
	LoginStatusCode     = 1
	LogoutCommand       = "logout"
	LogoutStatusCode    = 2
	JoinCommand         = "join"
	JoinStatusCode      = 3
	LeaveCommand        = "leave"
	LeaveStatusCode     = 4
	BroadcastCommand    = "broadcast"
	BroadcastStatusCode = 6
	SendCommand         = "send"
	SendStatusCode      = 7
	ListCommand         = "list"
	ListStatusCode      = 8

	//AlertStatusCode general server messages
	AlertStatusCode = 100
	//ConnectStatusCode new connection to room
	ConnectStatusCode = 201
	//DisconnectCode disconnected from room
	DisconnectCode = 499
	ClosedCode     = 444

	ServerMessage = "internal:server:msg"
)

type Command struct {
	Name        string
	Description string
	Code        int
	Min         int
	Max         int
	Whitelisted bool
}

type Protocol struct {
	commands  []*Command
	Whitelist []string
}

func (p *Protocol) Cmd() []*Command {
	return p.commands
}

func (p *Protocol) AddCommand(c *Command) {
	p.commands = append(p.commands, c)
	if c.Whitelisted {
		p.Whitelist = append(p.Whitelist, c.Name)
	}
}

func NewCommand(name, des string, code, len int, whitelisted bool) *Command {
	return &Command{
		Name:        name,
		Description: des,
		Code:        code,
		Whitelisted: whitelisted,
	}
}

func NewChatProtocol() *Protocol {
	chatProtocol := &Protocol{
		commands:  make([]*Command, 0),
		Whitelist: make([]string, 0),
	}

	chatProtocol.AddCommand(&Command{
		Name:        LoginCommand,
		Description: "login request, followed by [username] [password]",
		Code:        LoginStatusCode,
		Min:         2,
		Max:         2,
		Whitelisted: true,
	})
	chatProtocol.AddCommand(&Command{
		Name:        LogoutCommand,
		Description: "logout from session, closing connection & disconnectings from all rooms.",
		Code:        LogoutStatusCode,
		Min:         0,
		Max:         0,
		Whitelisted: true,
	})
	chatProtocol.AddCommand(&Command{
		Name:        JoinCommand,
		Description: "join a room, followed by [room] (optional [password])",
		Code:        JoinStatusCode,
		Min:         1,
		Max:         2,
		Whitelisted: true,
	})
	chatProtocol.AddCommand(&Command{
		Name:        LeaveCommand,
		Description: "leave a room",
		Code:        LeaveStatusCode,
		Min:         0,
		Max:         0,
		Whitelisted: true,
	})
	// /(broadcast) [room] [msg]
	chatProtocol.AddCommand(&Command{
		Name:        BroadcastCommand,
		Description: "broadcast message to user in room",
		Code:        BroadcastStatusCode,
		Min:         2,
		Max:         -1,
		Whitelisted: true,
	})
	// /(send) [room] [username] [msg]
	chatProtocol.AddCommand(&Command{
		Name:        SendCommand,
		Description: "Send a single messges to [room] [username]",
		Code:        SendStatusCode,
		Min:         3,
		Max:         -1,
		Whitelisted: true,
	})
	chatProtocol.AddCommand(&Command{
		Name:        ListCommand,
		Description: "List usernames in room",
		Code:        ListStatusCode,
		Min:         0,
		Max:         0,
		Whitelisted: true,
	})
	chatProtocol.AddCommand(&Command{
		Name:        "alert",
		Description: "alert message from server",
		Code:        AlertStatusCode,
		Min:         1,
		Max:         -1,
		Whitelisted: false,
	})
	return chatProtocol
}
