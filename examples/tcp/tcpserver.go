package main

import (
	"crypto/tls"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"time"

	socket "gitlab.com/krobolt/go-tcp"
)

const (
	network = "tcp"
	address = ":7018"
)

var (
	c                 chan os.Signal
	wait              time.Duration
	rootCertLocation  string
	serverKeyLocation string
)

func main() {
	flag.DurationVar(
		&wait,
		"graceful-timeout",
		time.Second*5,
		"the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m",
	)
	flag.StringVar(
		&rootCertLocation,
		"root",
		"./keys/cert.pem",
		"location of cert.pem root certificate",
	)
	flag.StringVar(
		&serverKeyLocation,
		"key",
		"./keys/key.pem",
		"location of key.pem server key",
	)
	flag.Parse()

	root := getKey(rootCertLocation)
	skey := getKey(serverKeyLocation)
	cer, err := tls.X509KeyPair(root, skey)

	if err != nil {
		log.Fatal(err)
	}

	f, err := os.OpenFile("server.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	log.SetOutput(f)

	config := &socket.Config{
		Address: address,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
			Certificates:       []tls.Certificate{cer},
		},
		ReadDeadline:  time.Minute * 40,
		WriteDeadline: time.Minute * 40,
		Wait:          wait,
		MaxIllegalOps: 5,
	}

	socket.NewTCPServer(config, socket.NewLogger(100))
}

func getKey(path string) []byte {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	key, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	return key
}
