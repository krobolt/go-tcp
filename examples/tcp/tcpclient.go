package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	socket "gitlab.com/krobolt/go-tcp"
)

func main() {
	client("tcp", ":7018")
}

//client Simple Client Connection
//Open the connection and read for output from the server.
//Connection not closed until program exits
func client(network, address string) {

	f, err := os.Open("./keys/cert.pem")
	root, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	f.Close()

	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM(root)

	if !ok {
		log.Fatal("failed to parse root certificate")
	}

	TLSConfig := &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            roots,
	}

	log.Println("starting client")
	conn, err := tls.Dial(network, address, TLSConfig)

	tcp := &socket.TCPSocket{
		WriteDeadline: time.Minute * 40,
		ReadDeadline:  time.Minute * 40,
	}

	defer conn.Close()

	//send data
	go func() {
		var b bytes.Buffer
		b.Write([]byte("/join lobby"))
		tcp.Write(conn, []byte("/join lobby"))
	}()

	//read data
	for {
		buf, err := tcp.Read(conn)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(buf))
	}
}
