package main

import (
	"crypto/x509"
	"flag"
	"time"

	lib "gitlab.com/krobolt/go-skeleton/crypto"
)

var (
	host       = flag.String("host", "", "Comma-separated hostnames and IPs to generate a certificate for")
	validFrom  = flag.String("start-date", "", "Creation date formatted as Jan 1 15:04:05 2011")
	validFor   = flag.Duration("duration", 365*24*time.Hour, "Duration that certificate is valid for")
	isCA       = flag.Bool("ca", false, "whether this cert should be its own Certificate Authority")
	rsaBits    = flag.Int("rsa-bits", 2048, "Size of RSA key to generate. Ignored if --ecdsa-curve is set")
	ecdsaCurve = flag.String("ecdsa-curve", "", "ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521")
)

func main() {
	flag.Parse()

	p := &lib.PEM{
		Host:        *host,
		ValidFrom:   *validFrom,
		ValidFor:    *validFor,
		IsCA:        false,
		RsaBits:     *rsaBits,
		EcdsaCurve:  *ecdsaCurve,
		IsGenerated: false,
		Template:    x509.Certificate{},
	}

	err := p.Generate("cert.pem", "key.pem")
	if err != nil {
		panic(err)
	}
}
