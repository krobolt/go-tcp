package main

import (
	"crypto/tls"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	socket "gitlab.com/krobolt/go-tcp"
)

const (
	network = "tcp"
	address = ":6068"
)

func main() {

	f, err := os.OpenFile("websocket.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	log.SetOutput(f)

	config := &socket.Config{
		Address:       address,
		TLSConfig:     &tls.Config{},
		ReadDeadline:  time.Minute * 40,
		WriteDeadline: time.Minute * 40,
		Wait:          time.Second * 10,
		MaxIllegalOps: 5,
	}

	SocketWriterReader := &socket.Websocket{
		Opcode:        0x1, //text 0x1, binary 0x2
		WriteDeadline: time.Minute * 40,
		ReadDeadline:  time.Minute * 40,
	}

	server := socket.NewServer(config, SocketWriterReader, socket.NewLogger(100))
	go server.Relay()

	serv := func(w http.ResponseWriter, r *http.Request) {
		conn, err := server.UpgradeRequest(w, r)
		if err != nil {
			panic(err)
		}
		go server.Handle(conn)
	}

	http.HandleFunc("/ping", heartbeat)
	http.HandleFunc("/", serv)
	log.Fatal(http.ListenAndServe(address, nil))
}

func heartbeat(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "ok")
}
