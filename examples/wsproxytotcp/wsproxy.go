package main

import (
	"crypto/tls"
	"crypto/x509"
	"log"
	"net/http"
	"time"

	socket "gitlab.com/krobolt/go-tcp"
	"golang.org/x/crypto/acme/autocert"
)

var (
	devMode = true
)

const (
	network   = "tcp"
	address   = ":6068"
	proxyaddy = ":7018"
	rootCert  = `-----BEGIN CERTIFICATE-----
MIIB+TCCAZ+gAwIBAgIJAL05LKXo6PrrMAoGCCqGSM49BAMCMFkxCzAJBgNVBAYT
AkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRn
aXRzIFB0eSBMdGQxEjAQBgNVBAMMCWxvY2FsaG9zdDAeFw0xNTEyMDgxNDAxMTNa
Fw0yNTEyMDUxNDAxMTNaMFkxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0
YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQxEjAQBgNVBAMM
CWxvY2FsaG9zdDBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABHGaaHVod0hLOR4d
66xIrtS2TmEmjSFjt+DIEcb6sM9RTKS8TZcdBnEqq8YT7m2sKbV+TEq9Nn7d9pHz
pWG2heWjUDBOMB0GA1UdDgQWBBR0fqrecDJ44D/fiYJiOeBzfoqEijAfBgNVHSME
GDAWgBR0fqrecDJ44D/fiYJiOeBzfoqEijAMBgNVHRMEBTADAQH/MAoGCCqGSM49
BAMCA0gAMEUCIEKzVMF3JqjQjuM2rX7Rx8hancI5KJhwfeKu1xbyR7XaAiEA2UT7
1xOP035EcraRmWPe7tO0LpXgMxlh2VItpc2uc2w=
-----END CERTIFICATE-----
`
)

func main() {

	//Get cert for proxy connection
	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(rootCert))
	if !ok {
		log.Fatal("failed to parse root certificate")
	}

	config := &socket.Config{
		Address: proxyaddy,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
			RootCAs:            roots,
		},
		ReadDeadline:  time.Minute * 40,
		WriteDeadline: time.Minute * 40,
		Wait:          time.Second * 5,
		MaxIllegalOps: 5,
	}

	socketWriter := &socket.Websocket{
		Opcode:        0x1, //text 0x1, binary 0x2
		WriteDeadline: time.Minute * 40,
		ReadDeadline:  time.Minute * 40,
	}

	server := socket.NewServer(config, socketWriter, socket.NewLogger(100))

	serv := func(w http.ResponseWriter, r *http.Request) {
		conn, err := server.UpgradeRequest(w, r)
		if err != nil {
			panic(err)
		}
		//proxy connection from this websocket server to another tcp connection
		//enabled the use of golang backend with browser front end.
		go server.BridgeWebsocketToTCP(conn, network, config.Address, config.TLSConfig)
	}

	if !devMode {
		//add cert information or use apache/nginx for wss://
		m := &autocert.Manager{
			Cache:  autocert.DirCache("secret-dir"),
			Prompt: autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(
				"wss.host.com", "www.host.com", "host.com"),
		}
		autocertmux := http.NewServeMux()
		autocertmux.HandleFunc("/", serv)
		autocertserv := &http.Server{
			Addr:      ":wss",
			TLSConfig: m.TLSConfig(),
			Handler:   autocertmux,
		}
		autocertserv.ListenAndServeTLS("", "")
	}

	http.HandleFunc("/", serv)
	log.Fatal(http.ListenAndServe(address, nil))
}
