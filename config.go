package socket

import (
	"crypto/tls"
	"time"
)

//Config information
//TODO Read/Write deadlines and pass config to NewServer save in server
type Config struct {
	Address       string
	TLSConfig     *tls.Config
	WriteDeadline time.Duration
	ReadDeadline  time.Duration
	Wait          time.Duration
	MaxIllegalOps int
}
