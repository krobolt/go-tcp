package socket

//TODO: update messageBuf size/remove global
var (
	messageMaxSize = 2048
	messageBuf     = 124
)

//Coms server commmunication chans
type Coms struct {
	Ping       chan Message
	Announce   chan Message
	Broadcast  chan Message
	Connect    chan *Connection
	Disconnect chan *Connection
	Close      chan string
}

//NewComs create default server commmunication chans
func NewComs() *Coms {
	return &Coms{
		make(chan Message, messageBuf),
		make(chan Message, messageBuf),
		make(chan Message, messageBuf),
		make(chan *Connection),
		make(chan *Connection),
		make(chan string),
	}
}
