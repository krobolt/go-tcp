package socket

import "encoding/json"

//Message used internally to share information
//between server and rooms
type Message struct {
	Room     string
	To       *Connection
	From     *Connection
	Data     string //cmds/internal server commands
	Code     int
	Contents []string
}

//Response to be sent to clients
type Response struct {
	Code     int      `json:"code"`
	Contents []string `json:"data"`
	Room     string   `json:"room"`
	From     string   `json:"from"`
}

//WriteMessage to socket
func WriteMessage(e Response) ([]byte, error) {
	return json.Marshal(e)
}
