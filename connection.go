package socket

import (
	"net"
	"time"
)

type health struct {
	IllegalOps int
}

//Connection information
type Connection struct {
	//net.Conn connection
	Conn     net.Conn
	Username string
	//subbed rooms
	Rooms         map[string]bool
	ReadDeadline  time.Duration
	WriteDeadline time.Duration
	Health        *health
}

//NewConnection returns Connection
func NewConnection(username string, c net.Conn) *Connection {
	return &Connection{
		Conn:     c,
		Rooms:    make(map[string]bool),
		Username: username,
		Health: &health{
			IllegalOps: 0,
		},
	}
}
