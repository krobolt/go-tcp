package socket

import (
	"bufio"
	"encoding/binary"
	"errors"
	"net"
	"time"
)

//TCPSocket implements WriterReaderRelay interface
type TCPSocket struct {
	WriteDeadline time.Duration
	ReadDeadline  time.Duration
}

//WriteBytes to socket
func (t *TCPSocket) Write(c net.Conn, b []byte) (int, error) {
	c.SetWriteDeadline(time.Now().Add(t.WriteDeadline))
	hb := createSocketHeader(len(b))
	out := append(hb, b...)
	return c.Write(out)
}

func (t *TCPSocket) Read(c net.Conn) ([]byte, error) {

	c.SetReadDeadline(time.Now().Add(t.ReadDeadline))
	cbuf := bufio.NewReader(c)
	datalen, err := readheader(cbuf)
	buf := make([]byte, int(datalen))

	if err != nil {
		return buf, err
	}

	_, err = cbuf.Read(buf)
	return buf, err
}

func createSocketHeader(leng int) []byte {
	hb := make([]byte, 2)
	binary.LittleEndian.PutUint16(hb, uint16(leng))
	return hb
}

func readheader(b *bufio.Reader) (int, error) {

	//get header/message size from uint16 LittleEndian bytes
	b1, err := b.ReadByte()
	if err != nil {
		return 0, errors.New("unable to read byte")
	}
	b2, err := b.ReadByte()
	if err != nil {
		return 1, errors.New("unable to read second byte")
	}

	datalen := binary.LittleEndian.Uint16([]byte{b1, b2})

	if datalen == 0 {
		return 1, errors.New("unable to read Uint16 header")
	}

	return int(datalen), err

}
