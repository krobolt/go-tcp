package socket

import (
	"net"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

//Websocket implements socket.WriterReader
type Websocket struct {
	Opcode        ws.OpCode
	WriteDeadline time.Duration
	ReadDeadline  time.Duration
}

//Write to socket casting error
func (w *Websocket) Write(c net.Conn, b []byte) (int, error) {
	c.SetWriteDeadline(time.Now().Add(w.WriteDeadline))
	err := wsutil.WriteServerMessage(c, w.Opcode, b)
	if err != nil {
		return 0, err
	}
	return len(b), err
}

//Read websocket and update deadline
func (w *Websocket) Read(c net.Conn) ([]byte, error) {
	c.SetReadDeadline(time.Now().Add(w.ReadDeadline))
	msg, _, err := wsutil.ReadClientData(c)
	return msg, err
}
