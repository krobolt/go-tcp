package socket

import (
	"testing"
)

func TestPackUnpack(t *testing.T) {

	encoder := NewTransport()

	msg := `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elit mi, scelerisque a sodales ac, vestibulum sed sem. 
	Integer eget hendrerit tortor, quis faucibus dui. Nam mi augue, gravida quis sapien a, tempus ullamcorper tortor. 
	Cras a pellentesque urna, ut mollis ex. Morbi posuere tortor et mi molestie, vitae molestie lorem blandit. Fusce ultricies bibendum arcu,
	a sollicitudin erat ornare a. Fusce mauris magna, consequat id sollicitudin sit amet, pharetra nec lorem. Vestibulum ac lectus nec
	felis maximus gravida quis sed neque. Mauris nibh diam, condimentum et iaculis vel, tincidunt sed ligula. Sed vitae tempor ex. 
	Pellentesque odio magna, commodo et diam in, fermentum semper magna.`

	enc, err := encoder.Pack(msg)

	if err != nil {
		t.Error(err.Error())
	}

	unpack, err := encoder.Unpack(enc)

	if err != nil {
		t.Error(err.Error())
	}

	if unpack != msg {
		t.Error("unpacked message is not the same")
	}

}
